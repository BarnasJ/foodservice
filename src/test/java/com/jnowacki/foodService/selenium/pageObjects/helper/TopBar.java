package com.jnowacki.foodService.selenium.pageObjects.helper;

import com.jnowacki.foodService.selenium.pageObjects.ActiveOrdersPage;
import com.jnowacki.foodService.selenium.pageObjects.RegisterPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class TopBar
{
    @FindBy(linkText = "Food Project")
    private WebElement homePageLink;

    @FindBy(id = "loginButton")
    private WebElement loginButton;

    @FindBy(id = "logoutButton")
    private WebElement logoutButton;

    @FindBy(id = "registerButton")
    private WebElement registerButton;

    @FindBy(name = "username")
    private WebElement loginField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @Autowired
    private ActiveOrdersPage activeOrdersPage;

    @Autowired
    private RegisterPage registerPage;

    @Autowired
    public TopBar(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public ActiveOrdersPage goToHomePage()
    {
        homePageLink.click();

        return activeOrdersPage;
    }

    public void putLogin(String login)
    {
        loginField.sendKeys(login);
    }

    public void putPassword(String password)
    {
        passwordField.sendKeys(password);
    }

    public void clickLogIn()
    {
        loginButton.click();
    }

    public RegisterPage goToRegistrationPage()
    {
        registerButton.click();

        return registerPage;
    }

    public boolean isLogged()
    {
        //Try to locate login button - if present users is not logged in if not - user is logged in
        try
        {
            loginButton.isDisplayed();
        }
        catch (Exception e)
        {
            return true;
        }

        return false;
    }
}

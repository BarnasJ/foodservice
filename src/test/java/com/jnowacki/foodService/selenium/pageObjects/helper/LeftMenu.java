package com.jnowacki.foodService.selenium.pageObjects.helper;

import com.jnowacki.foodService.selenium.pageObjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public final class LeftMenu
{
    @FindBy(linkText = "Aktywne")
    private WebElement activeOrdersLink;

    @FindBy(linkText = "Historia")
    private WebElement historyOrdersLink;

    @FindBy(linkText = "Utwórz zamówienie")
    private WebElement createOrderLink;

    @FindBy(linkText = "Przeglądaj menu")
    private WebElement listMenusLink;

    @FindBy(linkText = "Utwórz menu")
    private WebElement createMenuLink;

    @Autowired
    private ActiveOrdersPage activeOrdersPage;

    @Autowired
    private HistoryOrdersPage historyOrdersPage;

    @Autowired
    private CreateOrderPage createOrderPage;

    @Autowired
    private MenusListPage menusListPage;

    @Autowired
    private CreateMenuPage createMenuPage;

    @Autowired
    public LeftMenu(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public ActiveOrdersPage goToActiveOrders()
    {
        activeOrdersLink.click();

        return activeOrdersPage;
    }

    public HistoryOrdersPage goToHistoryOrders()
    {
        historyOrdersLink.click();

        return historyOrdersPage;
    }

    public CreateOrderPage goToCreateOrder()
    {
        createOrderLink.click();

        return createOrderPage;
    }

    public MenusListPage goToMenuList()
    {
        listMenusLink.click();

        return menusListPage;
    }

    public CreateMenuPage goToCreateMenu()
    {
        createMenuLink.click();

        return createMenuPage;
    }
}

package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class CreateOrderPage extends BasicPage
{
    @FindBy(id = "orderName")
    private WebElement orderNameField;

    @FindBy(id = "menus")
    private WebElement menusSelect;

    @FindBy(id = "endTime")
    private WebElement endTimeField;

    @FindBy(id = "owner")
    private WebElement ownerField;

    @FindBy(id = "comment")
    private WebElement commentField;

    @FindBy(id = "cashCheckbox")
    private WebElement cashCheckbox;

    @FindBy(id = "transferCheckbox")
    private WebElement transferCheckbox;

    @FindBy(id = "blikCheckbox")
    private WebElement blikCheckbox;

    @FindBy(id = "immediateCheckbox")
    private WebElement immediateCheckbox;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "saveButton")
    private WebElement saveButton;

    @FindBy(id = "nameError")
    private WebElement nameError;

    @FindBy(id = "endTimeError")
    private WebElement endTimeError;

    @FindBy(id = "ownerError")
    private WebElement ownerError;

    @FindBy(id = "commentError")
    private WebElement commentError;

    @FindBy(id = "paymentError")
    private WebElement paymentError;

    @FindBy(id = "passwordError")
    private WebElement passwordError;

    @Autowired
    private SummaryPage summaryPage;

    @Autowired
    public CreateOrderPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public void putOrderName(String name)
    {
        orderNameField.sendKeys(name);
    }

    public void putEndTime(String endTime)
    {
        endTimeField.sendKeys(endTime);
    }

    public void putOwner(String owner)
    {
        ownerField.sendKeys(owner);
    }

    public void putComment(String comment)
    {
        commentField.sendKeys(comment);
    }

    public void toggleCashChecbox()
    {
        cashCheckbox.click();
    }

    public void toggleTransferChecbox()
    {
        transferCheckbox.click();
    }

    public void toggleBlikChecbox()
    {
        blikCheckbox.click();
    }

    public void toggleImmediateChecbox()
    {
        immediateCheckbox.click();
    }

    public void putPassword(String password)
    {
        passwordField.sendKeys(password);
    }

    public SummaryPage clickSave()
    {
        saveButton.click();

        return summaryPage;
    }

    public String chooseRandomMenu()
    {
        Select select = new Select(menusSelect);
        int options = select.getOptions().size();

        Random r = new Random();

        int i = r.nextInt(options);

        select.selectByIndex(i);

        return select.getOptions().get(i).getText();
    }

    public boolean isNameErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            nameError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isEndTimeErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            endTimeError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isCommentErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            commentError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isPaymentErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            paymentError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isPasswordErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            passwordError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isOwnerErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            ownerError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public void navigate()
    {
        goToPage("newOrder");
    }

    public boolean isOnPage()
    {
        return isOnPage("/newOrder");
    }
}

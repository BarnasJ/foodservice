package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by jnowacki on 9/12/16.
 */

@Component
public class ItemListPage extends BasicPage
{
    @FindBy(id = "items-table")
    private WebElement itemsTable;

    @FindBy(id = "newItemButton")
    private WebElement newItemButton;

    @Autowired
    private CreateItemPage createItemPage;

    @Autowired
    public ItemListPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public List<String> getRows()
    {
        return getTableRowsTexts(itemsTable);
    }

    public CreateItemPage clickNewItemButton()
    {
        newItemButton.click();

        return createItemPage;
    }
}

package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class HistoryOrdersPage extends BasicPage
{

    @FindBy(id = "order-table")
    private WebElement ordersTable;

    @Autowired
    public HistoryOrdersPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public List<String> getRows()
    {
        return getTableRowsTexts(ordersTable);
    }

    public void navigate()
    {
        goToPage("orders?active=false");
    }
}

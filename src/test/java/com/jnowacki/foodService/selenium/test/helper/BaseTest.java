package com.jnowacki.foodService.selenium.test.helper;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by jnowacki on 9/11/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:seleniumTestContext.xml")
public class BaseTest
{
}

package com.jnowacki.foodService.selenium.test;

import com.jnowacki.foodService.selenium.pageObjects.ActiveOrdersPage;
import com.jnowacki.foodService.selenium.pageObjects.CreateItemPage;
import com.jnowacki.foodService.selenium.pageObjects.ItemListPage;
import com.jnowacki.foodService.selenium.test.helper.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;
import static org.junit.Assume.*;


import java.util.Date;

/**
 * Created by jnowacki on 9/12/16.
 */
public class ItemCreationTests extends BaseTest
{
    @Autowired
    private ActiveOrdersPage activeOrdersPage;

    @Before
    public void setUp()
    {
        activeOrdersPage.goToHomePage();
    }

    @Test
    public void createNewItemTest()
    {
        //data for creating item
        String owner = "owner" + new Date(); //Date for unique owner name
        String password = "qwe";

        //given
        activeOrdersPage.navigate();
        assumeTrue(activeOrdersPage.getRows().size() > 1); //Assume there are any orders - size 1 is table with header only
        CreateItemPage createItemPage = activeOrdersPage.enterFirstOrder().clickNewItemButton();

        //when
        createItemPage.putOwner(owner);
        createItemPage.putPassword(password);
        ItemListPage itemListPage = createItemPage.clickSave();

        //then
        boolean isOwnerInRow = false;

        for(String row : itemListPage.getRows())
        {
            if(row.contains(owner))
                isOwnerInRow = true;
        }

        assertTrue(isOwnerInRow);
    }
}

package com.jnowacki.foodService.selenium.test;

import com.jnowacki.foodService.selenium.pageObjects.CreateMenuPage;
import com.jnowacki.foodService.selenium.pageObjects.MenusListPage;
import com.jnowacki.foodService.selenium.test.helper.BaseTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

/**
 * Created by jnowacki on 9/11/16.
 */


public class MenuCreationPageTests extends BaseTest
{
    @Autowired
    private CreateMenuPage createMenuPage;

    @Autowired
    private MenusListPage menusListPage;

    @Before
    public void setUp()
    {
        createMenuPage.goToHomePage();
    }

    @Test
    public void createNewMenuTest()
    {
        //data for creating menu
        String name = "test menu " + new Date(); //Date for unique name
        String url = "http://test.url";
        String comment = "comment";

        String menuRow = name + " " + url + " " + comment;

        //given
        menusListPage.navigate();
        assumeFalse(menusListPage.getRows().contains(menuRow)); //assume there is no menu created with given data

        //when
        createMenuPage.navigate();
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        menusListPage.navigate();
        assertTrue(menusListPage.getRows().contains(menuRow));
    }

    @Test
    public void createNewMenuWithEmptyName()
    {
        //data for creating menu
        String name = "";
        String url = "http://test.url";
        String comment = "comment";

        //given
        createMenuPage.navigate();

        //when
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isNameErrorVisible());
    }

    @Test
    public void createNewMenuWithTooLongName()
    {
        //data for creating menu
        String name = RandomStringUtils.random(101, true, true); //name length is 101, use letters and numbers
        String url = "http://test.url";
        String comment = "comment";

        //given
        createMenuPage.navigate();

        //when
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isNameErrorVisible());
    }

    @Test
    public void createNewMenuWithEmptyUrl()
    {
        //data for creating menu
        String name = "test menu";
        String url = "";
        String comment = "comment";

        //given
        createMenuPage.navigate();

        //when
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isSourceUrlErrorVisible());
    }

    @Test
    public void createNewMenuWithMalformedUrl()
    {
        //data for creating menu
        String name = "test menu";
        String url = "malformedURL";
        String comment = "comment";

        //given
        createMenuPage.navigate();

        //when
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isSourceUrlErrorVisible());
    }

    @Test
    public void createNewMenuWithTooLongUrl()
    {
        //data for creating menu
        String name = "test menu";
        String url = "http://" + RandomStringUtils.random(131, true, true) + ".pl"; //name length is 131 (141 with protocol), use letters and numbers
        String comment = "comment";

        //given
        createMenuPage.navigate();

        //when
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isSourceUrlErrorVisible());
    }

    @Test
    public void createNewMenuWithDuplicatedName()
    {
        //data for creating menu
        String name = "test menu";
        String url = "http://test.url";
        String comment = "comment";

        String menuRow = name + " " + url + " " + comment;

        //given
        menusListPage.navigate();

        if(!menusListPage.getRows().contains(menuRow)) //if no menu existing with given name, create it
        {
            createMenuPage.navigate();
            createMenuPage.putRestaurantName(name);
            createMenuPage.putSourceUrl(url);
            createMenuPage.putComment(comment);

            createMenuPage.clickSave();
        }

        //when
        createMenuPage.navigate();
        createMenuPage.putRestaurantName(name);
        createMenuPage.putSourceUrl(url);
        createMenuPage.putComment(comment);

        createMenuPage.clickSave();

        //then
        assertTrue(createMenuPage.isNameErrorVisible());
    }
}

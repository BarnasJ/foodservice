package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.User;
import com.jnowacki.foodService.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jnowacki on 8/20/16.
 */

@Service
public class UsersService
{
    @Autowired
    private UsersRepository usersRepository;

    public List<User> getAll()
    {
        return (List<User>) usersRepository.findAll();
    }

    public User getByEmail(String email)
    {
        return usersRepository.findByEmail(email);
    }

    public User getEmpty()
    {
        return new User();
    }

    public User save(User user)
    {
        return usersRepository.save(user);
    }
}

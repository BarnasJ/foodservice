package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.repository.OrdersRepository;
import com.jnowacki.foodService.util.CalcUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * Created by jnowacki on 7/31/16.
 */

@Service
public class OrdersService
{
    @Autowired
    private OrdersRepository ordersRepository;

    public List<Order> getActive()
    {
        return ordersRepository.findOrderByEndTimeGreaterThanOrderByEndTimeAsc(new Date());
    }

    public List<Order> getInactive()
    {
        return ordersRepository.findOrderByEndTimeLessThanOrderByEndTimeDesc(new Date());
    }

    public Order getEmpty()
    {
        return new Order();
    }

    public Order save(Order order)
    {
        return ordersRepository.save(order);
    }

    public List<Order> getAll()
    {
        return (List<Order>) ordersRepository.findAll();
    }

    public Order getById(Long id)
    {
        return ordersRepository.findOne(id);
    }

    public boolean validate(Long orderId, String hashedPassword)
    {
        return hashedPassword != null && hashedPassword.equals(getById(orderId).getHash());

    }

}

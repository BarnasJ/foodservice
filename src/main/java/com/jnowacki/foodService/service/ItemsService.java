package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.OrderItem;
import com.jnowacki.foodService.repository.OrderItemsRepository;
import com.jnowacki.foodService.util.CalcUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jnowacki on 8/8/16.
 */

@Service
public class ItemsService
{
    @Autowired
    private OrderItemsRepository orderItemsRepository;

    public List<OrderItem> getFromOrder(Order order)
    {
        return orderItemsRepository.findByOrder(order);
    }

    public OrderItem getEmpty()
    {
        return new OrderItem();
    }

    public OrderItem saveItem(OrderItem orderItem)
    {
        return orderItemsRepository.save(orderItem);
    }

    public OrderItem getById(Long itemId)
    {
        return orderItemsRepository.findOne(itemId);
    }

    public boolean validate(Long itemId, String hashedPassword)
    {
        return hashedPassword != null && hashedPassword.equals(getById(itemId).getHash());
    }

}

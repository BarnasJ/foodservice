package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.UserRole;
import com.jnowacki.foodService.repository.UserRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jnowacki on 8/21/16.
 */

@Service
public class UserRolesService
{
    @Autowired
    private UserRolesRepository userRolesRepository;

    public UserRole getByName(String roleName)
    {
        return userRolesRepository.getByRoleName(roleName);
    }
}

package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.PaymentForm;
import com.jnowacki.foodService.repository.PaymentFormsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jnowacki on 8/8/16.
 */

@Service
public class PaymentFormService
{

    @Autowired
    private PaymentFormsRepository paymentFormsRepository;

    public List<PaymentForm> getEligible(Order order)
    {
        List<PaymentForm> eligiblePaymentForms= new ArrayList<>();

        if(order.isCashAllowed())
        {
            eligiblePaymentForms.add(paymentFormsRepository.findByName("Gotówka"));
        }
        if(order.isTransferAllowed())
        {
            eligiblePaymentForms.add(paymentFormsRepository.findByName("Przelew"));
        }
        if(order.isBlikAllowed())
        {
            eligiblePaymentForms.add(paymentFormsRepository.findByName("Blik"));
        }

        return eligiblePaymentForms;
    }
}

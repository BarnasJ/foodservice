package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.OrderItem;
import com.jnowacki.foodService.entity.User;
import com.jnowacki.foodService.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by jnowacki on 8/8/16.
 */

@Controller
public class OrderItemsController
{
    @Autowired
    private DishesService dishesService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ItemsService itemsService;

    @Autowired
    private PaymentFormService paymentFormService;

    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String returnItems(@RequestParam(value = "orderId") Long orderId, Model model)
    {
        Order order = ordersService.getById(orderId);

        model.addAttribute("message", "Zamówienie: " + order.getName());
        model.addAttribute("order", order);
        model.addAttribute("items", itemsService.getFromOrder(order));

        return "itemList";
    }

    @RequestMapping(value = "/newItem", method = RequestMethod.GET)
    public String newItem(@RequestParam(value = "orderId") Long orderId, Model model)
    {
        Order order = ordersService.getById(orderId);

        if (!model.containsAttribute("item"))
        {
            OrderItem orderItem = itemsService.getEmpty();
            orderItem.setOrder(order);

            model.addAttribute("item", orderItem);
        }

        model.addAttribute("message", "Dodaj danie do zamówienia");
        model.addAttribute("dishes", dishesService.getFromMenu(order.getMenuId()));
        model.addAttribute("order", order);
        model.addAttribute("paymentForms", paymentFormService.getEligible(order));

        return "createItem";
    }

    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.GET)
    public String editItem(@PathVariable(value = "itemId") Long itemId, Model model)
    {
        OrderItem orderItem = itemsService.getById(itemId);
        Order order = orderItem.getOrder();


        if (!model.containsAttribute("item"))
        {
            model.addAttribute("item", orderItem);
        }

        model.addAttribute("message", "Edytuj danie w zamówieniu");
        model.addAttribute("dishes", dishesService.getFromMenu(order.getMenuId()));
        model.addAttribute("order", order);
        model.addAttribute("paymentForms", paymentFormService.getEligible(order));

        return "createItem";
    }

    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.POST)
    public String itemPaidFor(@PathVariable(value = "itemId") Long itemId, @RequestParam(value = "paid") boolean isPaidFor)
    {
        OrderItem orderItem = itemsService.getById(itemId);
        Order order = orderItem.getOrder();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null &&
                order.getUser() != null &&
                authentication.getName().equals(order.getUser().getEmail()))
        {
            orderItem.setPaidFor(isPaidFor);

            itemsService.saveItem(orderItem);
        }

        return "redirect:/orders/summary/" + order.getId();
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public String saveItem(@ModelAttribute @Valid OrderItem item, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
//        If user not anonymous set user
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;

        if (authentication != null &&
                !authentication.getName().equals("anonymousUser"))
        {
            user = usersService.getByEmail(authentication.getName());
        }

        if (user == null && item.getHash() == null)
            bindingResult.rejectValue("hash", "NotNull");


        if (bindingResult.hasErrors())
        {
            item.setHash(null);

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.item", bindingResult);
            redirectAttributes.addFlashAttribute("item", item);

            if (item.getId() == null)
            {
                return "redirect:/newItem?orderId=" + item.getOrder().getId();
            }

            return "redirect:/items/" + item.getId();
        }

//        Get original item if exists
        OrderItem existingItem = null;
        if (item.getId() != null)
            existingItem = itemsService.getById(item.getId());

//        Check if new item is created or if existing one is being edited and user is authenticated or item is created anonymously but hash matches
        if (existingItem == null ||
                (existingItem.getUser() != null && existingItem.getUser().equals(user)) ||
                (existingItem.getUser() == null && itemsService.validate(item.getId(), item.getHash())))
        {
            if (existingItem == null && user != null)
                item.setUser(user);

            OrderItem newItem = itemsService.saveItem(item);

            return "redirect:/items?orderId=" + newItem.getOrder().getId();
        } else
        {
            String errorMessage;

            if (existingItem.getUser() != null)
                errorMessage = "To nie twoje zamówienie!";

            else
                errorMessage = "Błędne hasło!";

            item.setHash(null);
            redirectAttributes.addFlashAttribute("item", item);
            redirectAttributes.addFlashAttribute("errorMessage", errorMessage);

            return "redirect:/items/" + item.getId();
        }

    }
}

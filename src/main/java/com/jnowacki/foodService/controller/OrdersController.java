package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.User;
import com.jnowacki.foodService.service.ItemsService;
import com.jnowacki.foodService.service.MenusService;
import com.jnowacki.foodService.service.OrdersService;
import com.jnowacki.foodService.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by jnowacki on 7/31/16.
 */

@Controller
public class OrdersController
{
    @Autowired
    private OrdersService ordersService;

    @Autowired
    private MenusService menusService;

    @Autowired
    private UsersService usersService;

    @Autowired
    private ItemsService itemsService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String returnActiveOrders()
    {
        return "forward:/orders?active=true";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String returnOrders(@RequestParam(value = "active", required = false) boolean active, Model model)
    {
        if (active)
        {
            model.addAttribute("message", "Aktywne zamówienia");
            model.addAttribute("orders", ordersService.getActive());
        } else
        {
            model.addAttribute("message", "Historia zamówień");
            model.addAttribute("orders", ordersService.getInactive());
        }

        return "orderList";
    }

    @RequestMapping(value = "/newOrder", method = RequestMethod.GET)
    public String returnNewOrder(Model model)
    {
        if (!model.containsAttribute("order"))
        {
            model.addAttribute("order", ordersService.getEmpty());
        }

        model.addAttribute("message", "Utwórz nowe zamówienie");
        model.addAttribute("menus", menusService.getAll());

        return "createOrder";
    }

    @RequestMapping(value = "/orders/{orderId}", method = RequestMethod.GET)
    public String editOrder(@PathVariable(value = "orderId") Long orderId, Model model)
    {
        if (!model.containsAttribute("order"))
        {
            model.addAttribute("order", ordersService.getById(orderId));
        }

        model.addAttribute("message", "Edytuj zamówienie: " + ordersService.getById(orderId).getName());
        model.addAttribute("menus", menusService.getAll());

        return "createOrder";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public String saveOrder(@ModelAttribute @Valid Order order, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {

//        If user not anonymous set user
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;

        if (authentication != null &&
                !authentication.getName().equals("anonymousUser"))
        {
            user = usersService.getByEmail(authentication.getName());
        }

        if (user == null && order.getHash() == null)
            bindingResult.rejectValue("hash", "NotNull");

        //        Get original order if exists
        Order existingOrder = null;
        if (order.getId() != null)
        {
            existingOrder = ordersService.getById(order.getId());

//            Menu can't be changed if orders already exist
            if(itemsService.getFromOrder(order).size() > 0 && !existingOrder.getMenuId().getId().equals(order.getMenuId().getId()))
            {
                bindingResult.rejectValue("menuId", "typeMismatch");

                System.out.println(existingOrder);

                System.out.println(order);
            }
        }

        if (bindingResult.hasErrors())
        {
            order.setHash(null);

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.order", bindingResult);
            redirectAttributes.addFlashAttribute("order", order);

            if (order.getId() == null)
            {
                return "redirect:/newOrder";
            }

            return "redirect:/orders/" + order.getId();
        }

//        Check if new order is created or if existing one is being edited and user is authenticated or order is created anonymously but hash matches
        if (existingOrder == null ||
                (existingOrder.getUser() != null && existingOrder.getUser().equals(user)) ||
                (existingOrder.getUser() == null && ordersService.validate(order.getId(), order.getHash())))
        {
            if((existingOrder == null && user != null) || (existingOrder != null && existingOrder.getUser() != null))
                order.setUser(user);

            Order newOrder = ordersService.save(order);

            return "redirect:/orders/summary/" + newOrder.getId();
        } else
        {
            String errorMessage;

            if(existingOrder.getUser() != null)
                errorMessage = "Tylko zalogowany twórca zamówienia może je edytować";
            else
                errorMessage = "Błędne hasło!";

            order.setHash(null);
            redirectAttributes.addFlashAttribute("order", order);
            redirectAttributes.addFlashAttribute("errorMessage", errorMessage);

            return "redirect:/orders/" + order.getId();
        }
    }
}

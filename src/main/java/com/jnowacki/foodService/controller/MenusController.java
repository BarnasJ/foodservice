package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.entity.Menu;
import com.jnowacki.foodService.service.DishesService;
import com.jnowacki.foodService.service.MenusService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


/**
 * Created by jnowacki on 7/31/16.
 */

@Controller
public class MenusController
{
    @Autowired
    private MenusService menusService;

    @Autowired
    private DishesService dishesService;

    @RequestMapping("/menus")
    public String returnMenus(@RequestParam(value = "expandedMenuId", required = false) Long expandedMenuId, Model model)
    {
        if (expandedMenuId != null)
        {
            model.addAttribute("dishes", dishesService.getFromMenu(menusService.getById(expandedMenuId)));
            model.addAttribute("expandedMenuId", expandedMenuId);
        }

        model.addAttribute("message", "Dostępne menu");
        model.addAttribute("menus", menusService.getAll());

        return "menuList";
    }

    @RequestMapping(value = "/newMenu", method = RequestMethod.GET)
    public String returnNewMenu(Model model)
    {
        if (!model.containsAttribute("menu"))
        {
            model.addAttribute("menu", menusService.getEmpty());
        }

        model.addAttribute("message", "Utwórz nowe menu");

        return "createMenu";
    }

    @RequestMapping(value = "/menus/{menuId}", method = RequestMethod.GET)
    public String editMenu(@PathVariable(value = "menuId") Long menuId, Model model)
    {

        if (!model.containsAttribute("menu"))
        {
            model.addAttribute("menu", menusService.getById(menuId));
        }

        model.addAttribute("message", "Edytuj menu: " + menusService.getById(menuId).getName());
        model.addAttribute("dishes", dishesService.getFromMenu(menusService.getById(menuId)));

        return "createMenu";
    }

    @RequestMapping(value = "/menus", method = RequestMethod.POST)
    public String saveMenu(@ModelAttribute @Valid Menu menu, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
        if (!bindingResult.hasErrors())
        {
            try
            {
                Menu newMenu = menusService.save(menu);

                return "redirect:/menus/" + newMenu.getId();
            } catch (DataIntegrityViolationException e)
            {
                bindingResult.rejectValue("name", "typeMismatch");
            }

        }

        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.menu", bindingResult);
        redirectAttributes.addFlashAttribute("menu", menu);

        if (menu.getId() == null)
        {
            return "redirect:/newMenu";
        }

        return "redirect:/menus/" + menu.getId();


    }
}

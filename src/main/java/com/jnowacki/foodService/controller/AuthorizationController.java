package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.dto.RegistrationDto;
import com.jnowacki.foodService.entity.User;
import com.jnowacki.foodService.service.UserRolesService;
import com.jnowacki.foodService.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

/**
 * Created by jnowacki on 8/21/16.
 */

@Controller
public class AuthorizationController
{
    @Autowired
    private UsersService usersService;

    @Autowired
    private UserRolesService userRolesService;

    @RequestMapping(value = {"/logoutSuccess", "/loginSuccess"})
    public String pageAfterSuccess(HttpServletRequest request)
    {
        return getPreviousPageByRequest(request).orElse("/");
    }

    @RequestMapping("/login")
    public String loginPage(@RequestParam(value = "error", required = false) boolean error, Model model)
    {

//      Logged user is redirected to homepage
        if (SecurityContextHolder.getContext().getAuthentication() != null &&
                !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser"))
        {
            return "redirect:/";
        }

        if (error)
        {
            model.addAttribute("errorMessage", "Niepoprawne dane");
        }

        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registerUser(Model model)
    {
        //      Logged user is redirected to homepage
        if (SecurityContextHolder.getContext().getAuthentication() != null &&
                !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser"))
        {
            return "redirect:/";
        }

        if (!model.containsAttribute("registrationDto"))
        {
            model.addAttribute("registrationDto", new RegistrationDto());
        }

        model.addAttribute("message", "Zarejestruj użytkownika");

        return "createUser";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute @Valid RegistrationDto registrationDto, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
        if (!bindingResult.hasErrors())
        {
            try
            {
                User newUser = usersService.getEmpty();

                newUser.setEmail(registrationDto.getEmail());
                newUser.setPassword(registrationDto.getPassword());
                newUser.setRole(userRolesService.getByName("ROLE_USER"));

                usersService.save(newUser);

                redirectAttributes.addFlashAttribute("successMessage", "Użytkownik zarejestrowany");

                return "redirect:/login";

            } catch (DataIntegrityViolationException e)
            {
                bindingResult.rejectValue("email", "typeMismatch");
            }

        }

        registrationDto.setPassword(null);
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.registrationDto", bindingResult);
        redirectAttributes.addFlashAttribute("registrationDto", registrationDto);

        return "redirect:/registration";
    }

    private Optional<String> getPreviousPageByRequest(HttpServletRequest request)
    {
        return Optional.ofNullable(request.getHeader("Referer")).map(requestUrl -> "redirect:" + requestUrl);
    }

}

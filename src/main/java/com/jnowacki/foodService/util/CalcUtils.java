package com.jnowacki.foodService.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jnowacki on 8/13/16.
 */
public class CalcUtils
{
    public static String hashPasswordSHA256(String strToHash)
    {
        if(!(strToHash == null || strToHash.equals("")))
        {
            MessageDigest md = null;
            try
            {
                md = MessageDigest.getInstance("SHA-256");
                md.update(strToHash.getBytes("UTF-8"));

            } catch (NoSuchAlgorithmException | UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }

            assert md != null;
            byte[] digest = md.digest();

            return String.format("%064x", new java.math.BigInteger(1, digest));
        }
        else
        {
            return null;
        }
    }

    public static String hashPasswordBcrypt(String password)
    {
        if(!(password == null || password.equals("")))
        {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return passwordEncoder.encode(password);
        }
        else
        {
            return null;
        }
    }

    public static boolean isBcryptPasswordValid(String password, String hashedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        return passwordEncoder.matches(password, hashedPassword);
    }
}

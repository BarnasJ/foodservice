package com.jnowacki.foodService.util;

import com.jnowacki.foodService.entity.Order;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by jnowacki on 8/24/16.
 */

public class PaymentDeclaredValidator
        implements ConstraintValidator<PaymentDeclared, Object>
{

    @Override
    public void initialize(PaymentDeclared constraintAnnotation)
    {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context)
    {
        Order order = (Order) obj;
        return order.isBlikAllowed() || order.isTransferAllowed() || order.isCashAllowed();
    }
}
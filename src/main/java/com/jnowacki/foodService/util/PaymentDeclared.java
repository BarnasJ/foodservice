package com.jnowacki.foodService.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by jnowacki on 8/24/16.
 */

@Target({TYPE,ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PaymentDeclaredValidator.class)
@Documented
public @interface PaymentDeclared {
    String message() default "At least one payment method must be declared";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
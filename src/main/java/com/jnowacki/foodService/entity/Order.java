package com.jnowacki.foodService.entity;

import com.jnowacki.foodService.util.CalcUtils;
import com.jnowacki.foodService.util.PaymentDeclared;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by jnowacki on 7/31/16.
 */

@Entity
@Table(name = "orders")
@PaymentDeclared
public class Order
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "menu_id", nullable = false)
    private Menu menuId;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotNull
    @Future
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
    @Column(name = "end_time", nullable = false)
    private Date endTime;

    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "owner", nullable = false, length = 45)
    private String owner;

    @Size(max = 250)
    @Column(name = "comment", length = 250)
    private String comment;

    @Column(name = "is_cash_allowed", nullable = false)
    private boolean isCashAllowed;

    @Column(name = "is_transfer_allowed", nullable = false)
    private boolean isTransferAllowed;

    @Column(name = "is_blik_allowed", nullable = false)
    private boolean isBlikAllowed;

    @Column(name = "immediate_payment", nullable = false)
    private boolean isPaymentImmediate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Size(min = 1)
    @Column(name = "hash",  length = 64)
    private String hash;

    public Order()
    {
    }

    public Order(Menu menuId, String name, Date endTime, String owner, String comment, boolean isCashAllowed, boolean isTransferAllowed, boolean isBlikAllowed, boolean isPaymentImmediate, User user, String hash)
    {
        this.menuId = menuId;
        this.name = name;
        this.endTime = endTime;
        this.owner = owner;
        this.comment = comment;
        this.isCashAllowed = isCashAllowed;
        this.isTransferAllowed = isTransferAllowed;
        this.isBlikAllowed = isBlikAllowed;
        this.isPaymentImmediate = isPaymentImmediate;
        this.user = user;
        this.hash = hash;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Menu getMenuId()
    {
        return menuId;
    }

    public void setMenuId(Menu menuId)
    {
        this.menuId = menuId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public boolean isCashAllowed()
    {
        return isCashAllowed;
    }

    public void setCashAllowed(boolean cashAllowed)
    {
        isCashAllowed = cashAllowed;
    }

    public boolean isTransferAllowed()
    {
        return isTransferAllowed;
    }

    public void setTransferAllowed(boolean transferAllowed)
    {
        isTransferAllowed = transferAllowed;
    }

    public boolean isBlikAllowed()
    {
        return isBlikAllowed;
    }

    public void setBlikAllowed(boolean blikAllowed)
    {
        isBlikAllowed = blikAllowed;
    }

    public boolean isPaymentImmediate()
    {
        return isPaymentImmediate;
    }

    public void setPaymentImmediate(boolean paymentImmediate)
    {
        isPaymentImmediate = paymentImmediate;
    }

    public String getHash()
    {
        return hash;
    }

    public void setHash(String strToHash)
    {
        this.hash = CalcUtils.hashPasswordSHA256(strToHash);
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "Order{" +
                "id=" + id +
                ", menuId=" + menuId +
                ", name='" + name + '\'' +
                ", endTime=" + endTime +
                ", owner='" + owner + '\'' +
                ", comment='" + comment + '\'' +
                ", isCashAllowed=" + isCashAllowed +
                ", isTransferAllowed=" + isTransferAllowed +
                ", isBlikAllowed=" + isBlikAllowed +
                ", isPaymentImmediate=" + isPaymentImmediate +
                ", user=" + user +
                ", hash='" + hash + '\'' +
                '}';
    }
}

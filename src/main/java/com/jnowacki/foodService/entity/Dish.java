package com.jnowacki.foodService.entity;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * Created by jnowacki on 7/31/16.
 */

@Entity
@Table(name = "dishes")
public class Dish
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "menu_id", nullable = false)
    private Menu menuId;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotNull
    @DecimalMin(value = "0.00",inclusive = false)
    @DecimalMax(value = "200.00", inclusive = false)
    @Column(name = "price", nullable = false)
    private Float price;

    @Size(max = 250)
    @Column(name = "comment", length = 250)
    private String comment;

    public Dish()
    {
    }

    public Dish(Menu menuId, String name, Float price, String comment)
    {
        this.menuId = menuId;
        this.name = name;
        this.price = price;
        this.comment = comment;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Menu getMenuId()
    {
        return menuId;
    }

    public void setMenuId(Menu menuId)
    {
        this.menuId = menuId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Float getPrice()
    {
        return price;
    }

    public void setPrice(Float price)
    {
        this.price = price;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return "Dish{" +
                "id=" + id +
                ", menuId=" + menuId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", comment=" + comment +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (!menuId.getId().equals(dish.menuId.getId())) return false;
        if (!name.equals(dish.name)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = menuId.getId().hashCode();
        result = 31 * result + name.hashCode();

        return result;
    }
}

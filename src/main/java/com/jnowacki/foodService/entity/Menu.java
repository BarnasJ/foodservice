package com.jnowacki.foodService.entity;

import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by jnowacki on 7/31/16.
 */

@Entity
@Table(name = "menus")
public class Menu
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name", length = 100, nullable = false, unique = true)
    private String name;

    @NotNull
    @URL
    @Size(min = 1, max = 140)
    @Column(name = "source_url", length = 140, nullable = false)
    private String sourceUrl;

    @NotNull
    @Size(max = 250)
    @Column(name = "comment", length = 250)
    private String comment;

    public Menu()
    {
    }

    public Menu(String name, String sourceUrl, String comment)
    {
        this.name = name;
        this.sourceUrl = sourceUrl;
        this.comment = comment;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSourceUrl()
    {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl)
    {
        this.sourceUrl = sourceUrl;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}

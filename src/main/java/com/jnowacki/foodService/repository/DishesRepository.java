package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.Dish;
import com.jnowacki.foodService.entity.Menu;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jnowacki on 7/31/16.
 */
public interface DishesRepository extends CrudRepository<Dish, Long>
{
    List<Dish> findByMenuId(Menu menu);
}

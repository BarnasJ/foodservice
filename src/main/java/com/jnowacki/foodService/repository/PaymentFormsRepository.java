package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.PaymentForm;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jnowacki on 7/31/16.
 */
public interface PaymentFormsRepository extends CrudRepository<PaymentForm, Long>
{
    PaymentForm findByName(String name);
}

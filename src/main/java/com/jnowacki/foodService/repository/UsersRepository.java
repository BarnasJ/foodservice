package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jnowacki on 8/20/16.
 */

@Repository
public interface UsersRepository extends CrudRepository<User, Long>
{
    User findByEmail(String name);
}

package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by jnowacki on 7/31/16.
 */

@Repository
public interface OrdersRepository extends CrudRepository<Order, Long>
{
    List<Order> findAllByOrderByEndTimeAsc();

    List<Order> findOrderByEndTimeGreaterThanOrderByEndTimeAsc(Date date);
    List<Order> findOrderByEndTimeLessThanOrderByEndTimeDesc(Date date);
}

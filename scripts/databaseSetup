DROP DATABASE IF EXISTS `food_service`;

CREATE DATABASE `food_service`
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

USE `food_service`;

CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `users`(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL UNIQUE,
  `password` varchar(60) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role_id`) REFERENCES `user_roles`(`id`)
) ENGINE=InnoDB;

CREATE TABLE `payment_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL UNIQUE,
  `source_url` varchar(140) NOT NULL,
  `comment` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `dishes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `comment` varchar(250),
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `end_time` DATETIME NOT NULL,
  `owner` varchar(45) NOT NULL,
  `is_cash_allowed` tinyint(1) NOT NULL,
  `is_transfer_allowed` tinyint(1) NOT NULL,
  `is_blik_allowed` tinyint(1) NOT NULL,
  `immediate_payment` tinyint(1) NOT NULL,
  `comment` varchar(250),
  `user_id` int(10) unsigned,
  `hash` varchar(64),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `dish_id` int(10) unsigned NOT NULL,
  `owner` varchar(45) NOT NULL,
  `comment` varchar(250),
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `payment_form_id` int(10) unsigned NOT NULL,  
  `user_id` int(10) unsigned,
  `hash` varchar(64),
  `paid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`payment_form_id`) REFERENCES `payment_forms` (`id`)
) ENGINE=InnoDB;

INSERT INTO `user_roles` VALUES (1,'ROLE_USER'),(2,'ROLE_MODERATOR'),(3,'ROLE_ADMIN');

INSERT INTO `payment_forms` VALUES (3,'Blik'),(1,'Gotówka'),(2,'Przelew');

insert into `menus` (name, source_url, comment) value ('Jiangsu', 'http://pyszne.pl/jiangsu-skorzewo', 'Chińczyk, dowóz od 80zł');

insert into `dishes`(name, price, comment, menu_id) values 
('Specjalność Jiangsu', 17.50,'Kurczak w cieście z sosem pomidorowo-czosnkowym', 1),
('Ryż smażony z kurczakiem i warzywami', 16.00,'', 1),
('Makaron smażony z kurczakiem i warzywami', 17.00,'', 1),
('Makaron sojowy z kurczakiem i warzywami', 17.00,'', 1),
('Makaron ryżowy z kurczakiem i warzywami', 16.00,'', 1),
('Kurczak curry', 16.50,'', 1),
('Warzywa rozmaitości z kurczakiem', 16.50,'', 1),
('Kurczak słodko-kwaśny', 17.50,'', 1),
('Kurczak 5 smaków', 17.50,'', 1),
('Kurczak po syczuańsku', 17.50,'', 1),
('Kurczak w sosie sojowo-czosnkowym', 17.50,'', 1),
('Kurczak Gongbao na ostro', 18.50,'Z orzeszkami ziemnymi ', 1),
('Kurczak w sosie generała', 21.00,'', 1),
('Kurczak z bambusem i grzybami Mun', 17.50,'', 1),
('Kurczak po Pekińsku', 16.50,'', 1),
('Kurczak chili', 18.00,'', 1),
('Filet kurczaka smażony w cieście z warzywami', 17.50,'', 1),

('Wieprzowina w sosie czosnkowym na ostro', 18.50,'', 1),
('Wieprzowina na ostro', 17.50,'', 1),
('Wieprzowina w sosie słodko-kwaśnym', 18.50,'', 1),
('Wieprzowina w sosie ostrygowym', 19.00,'', 1),
('Wieprzowina w sosie kwaśno-pikantnym', 18.50,'', 1),
('Wieprzowina po pekińsku', 18.00,'', 1),
('Ryż smażony z wieprzowiną i warzywami', 17.00,'', 1),
('Makaron smażony z wieprzowiną i warzywami', 18.00,'', 1),
('Makaron sojowy z wieprzowiną i warzywami', 18.00,'', 1),
('Makaron wyżowy z wieprzowiną i warzywami', 18.00,'', 1),
('Warzywa rozmaitości z wieprzowiną', 17.00,'', 1),

('Wołowina z grzybami Mun', 19.50,'', 1),
('Wołowina Chilli', 19.50,'', 1),
('Wołowina w sosie czarnej soi na ostro', 19.50,'', 1),
('Wołowina z gorącego półmiska', 24.50,'', 1),
('Ryż smażony z wołowiną i warzywami', 18.00,'', 1),
('Makaron smażony z wołowiną i warzywami', 19.00,'', 1),
('Makaron sojowy z wołowiną i warzywami', 19.00,'', 1),
('Makaron ryżowy z wołowina i warzywami', 19.00,'', 1),
('Wołowina z warzywami i sosem sojowo-czosnkowym', 20.00,'', 1),

('Cielęcina z warzywami w pięciu kolorach', 16.00,'', 1),
('Cielęcina z gorącego półmiska', 17.00,'', 1),
('Cielęcina danie domowe', 25.00,'', 1),
('Cielęcina z warzywami i sosem sojowo-czosnkowym', 25.50,'', 1),

('Kaczka z czosnkiem na ostro po syczuańsku', 32.00,'', 1),
('Kaczka z sezamem', 33.00,'', 1),
('Kaczka po Pekińsku', 32.00,'', 1),

('Filet z ryby smażony w cieście z warzywami', 18.00,'', 1),
('Halibut na parze po Katalońsku', 23.00,'', 1),
('Ryba w cieście w sosie słodko-kwaśnym', 19.50,'', 1),

('Krewetki Gongbao na ostro z orzechami', 25.00,'', 1),
('Małże w sosie ostrygowym', 21.00,'', 1),
('Łosoś na ostro', 24.50,'', 1),
('Kalmary Gongbao na ostro', 29.50,'', 1),
('Kalmary z warzywami', 29.00,'', 1),
('Duże krewetki z warzywami w pięciu kolorach', 35.00,'', 1),
('Duże krewetki z gorącego półmiska', 36.00,'', 1),

('Warzywa rozmaitości wegetariańskie', 15.50,'', 1),
('Ryż smażony z warzywami', 15.00,'', 1),
('Makaron smażony z warzywami', 16.00,'', 1),
('Makaron sojowy z warzywami', 16.00,'', 1),
('Makaron ryżowy z warzywami', 16.00,'', 1),
('Brokuły w sosie czosnkowym na ostro', 15.50,'', 1),
('Smażone grzyby Mun', 14.00,'', 1),
('Bambus z pieczarkami pachnącymi', 15.00,'', 1),
('Pędy bambusa z grzybami Mun i kiełkami', 15.50,'', 1),

('Zupa Wonton', 9.00,'', 1),
('Zupa pikantno-kwaśna', 9.00,'', 1),
('Zupa z kurczaka z bambusem', 8.00,'', 1),
('Zupa Jiangsu', 9.00,'', 1),
('Zupa curry z makaronem sojowym i wołowiną', 10.00,'', 1),
('Rosół chiński', 8.00,'', 1),

('Pierożki smażone', 8.00,'', 1),
('Krążki kalmara w cieście', 11.00,'', 1),
('Kurczak w cieście', 10.00,'', 1),
('Krewetki w cieście', 10.00,'', 1),
('Sajgonki 3 szt.', 8.00,'', 1),
('Sajgonki w cieście 3 szt.', 9.00,'', 1),
('Paluszki krabowe', 8.00,'', 1),
('Duże krewetki w tempurze 4 szt.', 17.00,'', 1),
('Chipsy krewetkowe', 6.00,'', 1),

('Surówka z kapusty pikantno-kwaśnej', 4.00,'', 1),
('Ogórki słodko-kwaśne', 6.00,'', 1),

('Ryż biały', 3.00,'', 1),
('Ryż zasmażany z warzywami', 6.00,'', 1),
('Makaron zasmażany', 6.00,'', 1),
('Makaron ryżowy', 6.00,'', 1),
('Makaron sojowy', 6.00,'', 1),
('Frytki', 7.00,'', 1),

('Banan w cieście', 8.00,'', 1),
('Ananas w cieście', 8.00,'', 1),
('Mars w cieście', 8.00,'', 1),
('Ciasteczko z wróżbą', 1.00,'', 1),

('Pepsi 1 L', 6.00,'', 1),

('Nigiri z łososiem 2 szt.', 14.00,'', 1),
('Nigiri z tuńczykiem 2szt.', 15.00,'', 1),
('Nigiri z krewetką 2szt.', 14.00,'', 1),
('Nigiri z awokado 2szt.', 13.00,'', 1),
('Nigiri z marynowaną rzepą 2szt.', 13.00,'', 1),

('Hosomaki Sake maki 8 szt.', 14.00,'', 1),
('Hosomaki Tuna maki 8 szt.', 15.00,'', 1),
('Hosomaki Ebi maki 8 szt.', 14.00,'', 1),
('Hosomaki Kanni kama maki 8 szt.', 11.00,'', 1),
('Hosomaki Avokado maki 8 szt.', 11.00,'', 1),
('Hosomaki Kappa maki 8 szt.', 11.00,'', 1),
('Hosomaki Oshinoko maki 8 szt.', 12.00,'', 1),

('Futomaki Sake philadelphia maki 6 szt.', 18.00,'', 1),
('Futomaki Sake philadelphia ten maki 6 szt.', 21.00,'', 1),
('Futomaki Tuna maki 6 szt.', 20.00,'', 1),
('Futomaki Ebi maki 6 szt.', 19.00,'', 1),
('Futomaki Spicy ebi maki 6szt.', 21.00,'', 1),
('Futomaki Kanni kama maki 6 szt.', 17.00,'', 1),
('Futomaki Kanni kama ten maki 6 szt.', 19.00,'', 1),
('Futomaki wegetariańskie 6 szt.', 16.00,'', 1),

('California Maki Alaska 4 szt.', 19.00,'', 1),
('California Maki Alaska ten 4 szt.', 21.00,'', 1),
('California Maki Spicy tuna 4 szt.', 21.00,'', 1),
('California Maki Ebi 4 szt.', 19.00,'', 1),
('California Maki Spicy Ebi ten 4 szt.', 22.00,'', 1),
('California Maki Rainbow 4 szt.', 18.00,'', 1),
('California Maki Rainbow ten 4 szt.', 20.00,'', 1),
('California Maki Wegetariańskie 4 szt.', 16.00,'', 1),

('Gunkan Maki Tobbiko red 2 szt.', 23.00,'', 1),
('Gunkan Maki Kani mayo 2 szt.',25.00 ,'', 1),
('Gunkan Maki Ebi mayo 2 szt.', 24.00,'', 1),
('Gunkan Maki Tobbiko black', 28.00,'', 1),

('Nigiri zestaw I 10 szt.', 60.00,'', 1),
('Jiangsu zestaw II 8 szt.', 29.00,'', 1),
('Slim zestaw III 14 szt.', 44.00,'', 1),
('Kamen zestaw IV 9 szt.', 43.00,'', 1),
('Wegetariańskie zestaw V 12 szt.', 34.00,'', 1),
('Gunkan zestaw VI 8 szt.', 86.00,'', 1),
('Tempura zestaw VII 16 szt.', 56.00,'', 1),
('Yoko zestaw VII 52 szt.', 160.00,'', 1);